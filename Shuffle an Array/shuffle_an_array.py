import random as radnom
import copy as cp
class Solution:

    def __init__(self, nums: List[int]):
        self.normal_nums = cp.deepcopy(nums)
        self.shuffle_list = nums

    def reset(self) -> List[int]:
        return self.normal_nums 

    def shuffle(self) -> List[int]:
        result_list = []
        while len(self.shuffle_list) > 0:
            chosen_element =  random.choice(self.shuffle_list)
            self.shuffle_list.remove(chosen_element)
            result_list.append(chosen_element)
        self.shuffle_list = result_list
        return result_list
        


# Your Solution object will be instantiated and called as such:
# obj = Solution(nums)
# param_1 = obj.reset()
# param_2 = obj.shuffle()
