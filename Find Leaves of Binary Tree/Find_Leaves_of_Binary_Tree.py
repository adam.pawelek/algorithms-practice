# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
import copy as cp
class Solution:
    def __init__(self):
        self.binary_tree=[]
        self.list_number = 0
    def findLeaves(self, root: Optional[TreeNode]) -> List[List[int]]:
        while (root.left or root.right):
            self.binary_tree.append([])
            self.last_level(root)
            self.list_number+=1
        self.binary_tree.append([root.val])
        #print (self.binary_tree)
        return self.binary_tree
            
            
        
    def last_level(self, root: Optional[TreeNode]):
        if root.left == None and root.right == None:
            self.binary_tree[self.list_number].append(root.val)
            root = None
            return 1
        if root.left:
            result = self.last_level(root.left)
            if result == 1:
                root.left = None
        if root.right:
            result = self.last_level(root.right)
            if result == 1:
                root.right = None
        return 0
            
        
        
