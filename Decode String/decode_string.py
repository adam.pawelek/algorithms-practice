class Solution:
    def __init__(self):
        self.brackets = ["[","]"]
    def decodeString(self, s: str) -> str:
        s = "[" + s
        s += "]"
        return self.string_recurion(s,1)[1]

            
    def string_recurion(self, s,i):
        result_string = ""
        number = ""
        while(i < len(s)):
            if ord(s[i]) >= 97 and ord(s[i]) <= 122:
                result_string += s[i]
            if ord(s[i]) >= 48 and ord(s[i]) <= 57:
                number+=s[i]
            
            if s[i] == "]":
                return[i,result_string]
            if s[i] == "[":
                return_list = self.string_recurion(s,i+1)
                i = return_list[0]
                result_string += int(number) * return_list[1] 
                number = ""
   
            i+=1
                
        return [i,result_string]
                
                
